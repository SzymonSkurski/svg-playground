package main

import (
	"fmt"
	"math/rand"
	"net/url"
	"time"
)

func randomColorHash() string {
	var rnd = rand.New(rand.NewSource(time.Now().UnixNano()))

	return fmt.Sprintf("%x%x%x%x%x%x", rnd.Intn(16), rnd.Intn(16), rnd.Intn(16), rnd.Intn(16), rnd.Intn(16), rnd.Intn(16))
}

func randomBoolean() bool {
	return randomN(0,1) == 1
}

func randomN(min, max int) int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return r.Intn(max-min+1) + min
}

func getRandomQueryParams() url.Values {
	// set query params
	u := url.Values{}
	u.Add("base", randomColorHash())
	u.Add("sleeve", randomColorHash())
	u.Add("number", randomColorHash())
	u.Add("squares", fmt.Sprintf("%v", randomBoolean()))
	u.Add("stripes", fmt.Sprintf("%v", randomBoolean()))
	u.Add("horizontal_stripes", fmt.Sprintf("%v", randomBoolean()))
	u.Add("split", fmt.Sprintf("%v", randomBoolean()))
	u.Add("shirt_type", "short_sleeves")
	u.Add("sleeve_detail", randomColorHash())
	u.Add("v_neck", fmt.Sprintf("%v", randomBoolean()))
	u.Add("l_sleeve", fmt.Sprintf("%v", randomBoolean()))

	return u
}
