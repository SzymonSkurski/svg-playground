package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	svg "github.com/ajstarks/svgo"
	"github.com/gin-gonic/gin"
)

// type="home" base="e41e2c" sleeve="e41e2c" number="ffffff" squares="false" stripes="false" horizontal_stripes="false" split="false" shirt_type="short_sleeves" sleeve_detail="ffffff"

type JerseySpec struct {
	BaseColor            string `form:"base"`
	SleeveColor          string `form:"sleeve"`
	NumberColor          string `form:"number"`
	HasSquares           bool   `form:"squares"`
	HasVerticalStripes   bool   `form:"stripes"`
	HasHorizontalStripes bool   `form:"horizontal_stripes"`
	IsSplit              bool   `form:"split"`
	Type                 string `form:"shirt_type"`
	SleeveDetailColor    string `form:"sleeve_detail"`
	HasVNeck             bool   `form:"v_neck"`
	Size                 int    `form:"size"`
	LongSleeve           bool   `form:"l_sleeve"`
	WriteSpec 			 bool 	`form:"spec"`
}

func newJerseySpec() *JerseySpec {
	return &JerseySpec{
		BaseColor:            "ffffff",
		SleeveColor:          "ffffff",
		NumberColor:          "#000000",
		HasSquares:           false,
		HasVerticalStripes:   false,
		HasHorizontalStripes: false,
		IsSplit:              false,
		Type:                 "short_sleeves",
		SleeveDetailColor:    "#ffffff",
		Size:                 300,
		LongSleeve: 		  false,
	}
}

type Dimensions struct {
	Size            int
	Width           int
	Height          int
	ShirtBaseWidth  int
	ShirtBaseHeight int
}

func (d *Dimensions) Set(size int) {
	d.Size = size
	d.Width = size
	d.Height = size
	d.ShirtBaseWidth = int(float64(size) * 0.5)
	d.ShirtBaseHeight = int(float64(size) * 0.9)
}

var dim = Dimensions{}
var strokeColor = "#e8e8e8"

func main() {
	r := gin.Default()
	r.GET("/svg", svgHandler)
	r.GET("/", homeHandler)
	r.GET("/svg/random", svgHandlerRandom)
	_ = r.Run(":9013") // listen and serve on 0.0.0.0:9013
}

func homeHandler(c *gin.Context) {
	readFile, err := os.Open("README.md")
	if err != nil {
		fmt.Println(err)
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(readFile)

	fileScanner := bufio.NewScanner(readFile)

	fileScanner.Split(bufio.ScanLines)

	for fileScanner.Scan() {
		_, _ = c.Writer.Write(fileScanner.Bytes())
	}
}

func svgHandler(c *gin.Context) {
	c.Header("Content-Type", "image/svg+xml")
	// read request data
	js := newJerseySpec()
	if err := c.ShouldBindQuery(js); err != nil {
		log.Fatal(err)
		return
	}
	setDimensions(js)
	generateJerseySVG(c, js)
}

func svgHandlerRandom(c *gin.Context) {
	c.Header("Content-Type", "image/svg+xml")

	u := getRandomQueryParams()

	c.Request.URL.RawQuery = u.Encode()
	// read request data
	js := newJerseySpec()
	if err := c.ShouldBindQuery(js); err != nil {
		log.Fatal(err)
		return
	}
	// spec should be write on image
	js.WriteSpec = true

	setDimensions(js)
	generateJerseySVG(c, js)
}

func setDimensions(js *JerseySpec) {
	if s := js.Size; s > 0 {
		dim.Set(s)
	}
}

func generateJerseySVG(c *gin.Context, js *JerseySpec) {
	s := svg.New(c.Writer)
	s.Start(dim.Width, dim.Height)
	generateSleeves(s, js)
	generateBase(s, js.BaseColor)
	generateStripes(s, js)
	splitShirt(s, js)
	writeSpec(s, js)
	generateNeckLine(s, js.HasVNeck, "#fff")
	s.End()
}

func generateBase(s *svg.SVG, fillColor string) {
	color := makeColorString(fillColor, strokeColor)
	x := centerX() - dim.ShirtBaseWidth/2
	y := centerY() - dim.ShirtBaseHeight/2

	s.Rect(x, y, dim.ShirtBaseWidth, dim.ShirtBaseHeight, color)
}

func generateSleeves(s *svg.SVG, js *JerseySpec) {
	if js.LongSleeve {
		generateSleevesLong(s, js)
		return
	}

	generateSleevesShort(s, js)
}

func generateSleevesLong(s *svg.SVG, js *JerseySpec) {
	colorR := makeColorString(js.SleeveColor, strokeColor)
	colorL := colorR
	if js.IsSplit {
		colorL = makeColorString(js.BaseColor, strokeColor)
	}

	y1 := centerY() - dim.ShirtBaseHeight/2 // sleeve top
	y2 := centerY() - dim.ShirtBaseHeight/2 + dim.ShirtBaseHeight/22 // sleeve outer top
	y3 := y1 + dim.ShirtBaseHeight / 4 // sleeve underarm
	y4 := dim.ShirtBaseHeight / 2 // sleeve elbow
	y5 := dim.ShirtBaseHeight // sleeve bottom
	xl1 := centerX() - dim.ShirtBaseWidth/2 // left sleeve start
	xl2 :=  xl1 - dim.ShirtBaseWidth / 6 // left sleeve first half end
	xr1 := centerX() + dim.ShirtBaseWidth/2 // right sleeve start
	xr2 :=  xr1 + dim.ShirtBaseWidth / 6 

	tht := dim.ShirtBaseWidth / 4 //sleeve thick top
	th := dim.ShirtBaseWidth / 5 // sleeve thick

	Ys := []int{y1, y3, y4, y5, y5, y4, y2, y1}
	// left sleeve
	leftXs := []int{xl1, xl1, xl2, xl1, xl1-th, xl2-th, xl1-tht, xl1} 
	s.Polygon(leftXs, Ys, colorL)
	// right sleeve
	rightXs := []int{xr1, xr1, xr2, xr1, xr1+th, xr2+th, xr1+tht, xr1} 
	s.Polygon(rightXs, Ys, colorR)
}

func generateSleevesShort(s *svg.SVG, js *JerseySpec) {
	colorR := makeColorString(js.SleeveColor, strokeColor)
	colorL := colorR
	if js.IsSplit {
		colorL = makeColorString(js.BaseColor, strokeColor)
	}

	y1 := centerY() - dim.ShirtBaseHeight/2 // sleeve top
	y2 := centerY() - dim.ShirtBaseHeight/2 + dim.ShirtBaseHeight/22 // sleeve outer top
	y3 := y1 + dim.ShirtBaseHeight / 4 // sleeve underarm
	y4 := dim.ShirtBaseHeight / 2 // sleeve elbow
	xl1 := centerX() - dim.ShirtBaseWidth/2 // left sleeve start
	xl2 :=  xl1 - dim.ShirtBaseWidth / 10 // left sleeve first half end
	xr1 := centerX() + dim.ShirtBaseWidth/2 // right sleeve start
	xr2 :=  xr1 + dim.ShirtBaseWidth / 10 

	tht := dim.ShirtBaseWidth / 4 //sleeve thick top
	th := dim.ShirtBaseWidth / 5 // sleeve thick

	Ys := []int{y1, y3, y4, y4, y2, y1}
	// left sleeve
	leftXs := []int{xl1, xl1, xl2, xl2-th, xl1-tht, xl1} 
	s.Polygon(leftXs, Ys, colorL)
	// right sleeve
	rightXs := []int{xr1, xr1, xr2, xr2+th, xr1+tht, xr1} 
	s.Polygon(rightXs, Ys, colorR)
}

func generateNeckLine(s *svg.SVG, vNeck bool, baseColor string) {
	w := dim.ShirtBaseWidth / 3
	y := centerY() - dim.ShirtBaseHeight/2 // shirt base top
	x1 := centerX() - w/2                  // neck left
	x2 := centerX() + w/2                  // neck right
	color := makeColorString(baseColor, strokeColor)

	s.Line(x1, y, x2, y, color)
	if vNeck {
		vn := y + w/2 // v-neck point
		// []{x1,x2,x3,x4},{y1,y2,y3,y4}
		s.Polyline([]int{x1, centerX(), x2, x1}, []int{y, vn, y, y}, color)
		return
	}
	// round neckline
	s.Arc(x1, y, 5, 5, w, false, false, x2, y, color)
}

func splitShirt(s *svg.SVG, js *JerseySpec) {
	if !js.IsSplit {
		return
	}
	st := 0  // space from base top
	h := dim.ShirtBaseHeight - st // stripes rect height
	w := dim.ShirtBaseWidth / 2
	x := centerX() - dim.ShirtBaseWidth/2
	y := centerY() - dim.ShirtBaseHeight/2 + st
	color := makeColorString(js.SleeveColor, "")

	s.Rect(x, y, w, h, color)
}

func writeSpec(s *svg.SVG, js *JerseySpec) {
	if !js.WriteSpec {
		return
	}

	x := 10
	y := dim.Size
	f1 := "base:#%s sleeve:#%s details:#%s"
	txt1 := fmt.Sprintf(f1, js.BaseColor, js.SleeveColor, js.SleeveDetailColor)
	f2 := "str:%v h_str:%v sqr:%v, spl:%v"
	txt2 := fmt.Sprintf(f2,js.HasVerticalStripes, js.HasHorizontalStripes, js.HasSquares, js.IsSplit)

	s.Text(x, y-14, txt1)
	s.Text(x, y-2, txt2)
}

func generateStripes(s *svg.SVG, js *JerseySpec) {
	if js.HasHorizontalStripes {
		generateHorizontalStripes(s, js.SleeveColor)
		return
	}
	if js.HasVerticalStripes {
		generateVerticalStripes(s, js.SleeveColor)
		return
	}
	if js.HasSquares {
		generateSquares(s, js.SleeveColor)
	}
}

func generateHorizontalStripes(s *svg.SVG, fillColor string) {
	c := 16                        // how many stripes
	st := 0   // space from base top
	sh := dim.ShirtBaseHeight - st // stripes rect height
	x := centerX() - dim.ShirtBaseWidth/2
	y := centerY() - dim.ShirtBaseHeight/2 + st
	h := sh / c // single stripe height
	w := dim.ShirtBaseWidth
	color := makeColorString(fillColor, "")
	for i := 0; i < c; i++ {
		if i%2 == 0 {
			continue
		}
		s.Rect(x, y+h*i, w, h, color)
	}
}

func generateVerticalStripes(s *svg.SVG, fillColor string) {
	c := 15                        // how many stripes
	st := 0   // space from base top
	sh := dim.ShirtBaseHeight - st // stripes rect height
	x := centerX() - dim.ShirtBaseWidth/2
	y := centerY() - dim.ShirtBaseHeight/2 + st
	h := sh // single stripe height
	w := dim.ShirtBaseWidth / c

	color := makeColorString(fillColor, "")
	for i := 0; i < c; i++ {
		if i%2 == 0 {
			continue
		}
		s.Rect(x+w*i, y, w, h, color)
	}
}

func generateSquares(s *svg.SVG, fillColor string) {
	// always cut into 15 rows

	cols := 15    // how many columns
	rows := 23 // how many rows
	h := dim.ShirtBaseHeight / rows
	w := dim.ShirtBaseWidth / cols
	spaceY := (dim.ShirtBaseHeight - h * rows) / 2
	spaceX := (dim.ShirtBaseWidth - w * cols) / 2
	x := centerX() - dim.ShirtBaseWidth/2 + spaceX
	y := centerY() - dim.ShirtBaseHeight/2 + spaceY
	color := makeColorString(fillColor, "")

	// fill top and bottom gaps makeColorString("ff0019", strokeColor)
	fillGapTop(s, cols, x, y, w, spaceY, color)
	fillGapBottom(s, cols, rows, x,y,w,spaceY, color)
	fillGapLeft(s, rows, x, y, h, spaceX, color)
	fillGapRight(s, rows, cols,  x, y, h, spaceX, color)

	for col := 0; col < cols; col++ {
		space := 0
		if col%2 == 0 {
			space = 1 
		}
		for row := 0; row < rows; row++ {
			// square every second column
			if row%2 == space {
				continue
			}
			sx := x+w*col
			sy := y+h*row

			s.Rect(sx, sy, w, h, color)
		}
	}
}

func fillGapRight(s *svg.SVG, rows, cols, x, y, h, spaceX int, color string) {
	if spaceX < 1 {
		return
	}
	sx := centerX() + dim.ShirtBaseWidth/2 - spaceX
	space := 0
	if cols % 2 == 0 {
		space = 1
	}

	for row := 0; row < rows; row++ {
		// square every second column
		if row%2 == space {
			continue
		}
		
		sy := y+h*row

		s.Rect(sx, sy, spaceX, h, color) // left gap
	}
}

func fillGapLeft(s *svg.SVG, rows, x, y, h, spaceX int, color string) {
	if spaceX < 1 {
		return
	}
	sx := centerX() - dim.ShirtBaseWidth/2

	for row := 0; row < rows; row++ {
		// square every second column
		if row%2 == 0 {
			continue
		}
		
		sy := y+h*row

		s.Rect(sx, sy, spaceX, h, color) // left gap
	}
}

func fillGapTop(s *svg.SVG, cols, x, y, w, spaceY int, color string) {
	if spaceY < 1 {
		return
	}
	sy := centerY() - dim.ShirtBaseHeight/2
	for col := 0; col < cols; col++ {
		if col%2 == 0 {
			continue
		}
		sx := x+w*col
		
		s.Rect(sx, sy, w, spaceY, color) // top gap
	}
}

func fillGapBottom(s *svg.SVG, cols, rows, x, y, w, spaceY int, color string) {
	if spaceY < 1 {
		return
	}
	space := 0 
	if rows % 2 == 0 {
		space = 1
	}
	sy := centerY() + dim.ShirtBaseHeight/2 - spaceY

	for col := 0; col < cols; col++ {
		if col%2 == space {
			continue
		}
		sx := x+w*col
		
		s.Rect(sx, sy, w, spaceY, color) // bottom gap
	}
}

func makeColorString(fillColor, strokeColor string) string {
	fillColor = getRidOffHash(fillColor)
	strokeColor = getRidOffHash(strokeColor)
	return fmt.Sprintf("fill:#%s;stroke:#%s", fillColor, strokeColor)
}

func getRidOffHash(color string) string {
	s := strings.Split(color, "#")
	if len(s) == 0 {
		return color
	}
	return s[len(s)-1]
}

func centerX() int {
	return dim.Width / 2
}

func centerY() int {
	return dim.Height / 2
}
