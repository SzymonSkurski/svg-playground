# svg-playground

Simple SVG shirts generator

## run
clone repository `git clone https://gitlab.com/SzymonSkurski/svg-playground.git`  
move to project folder `cd svg-playground`  
install go packages `go mod tidy`  
run `go run main.go`  
open browser `http://localhost:9013/svg`

## GET params
`base` - shirt base color in string format, `?base=ffff00`  
`sleeve` - shirt sleeve color in string format `?sleeve=000000`  
`number` - number color in string format **not implemented** 
  
**notice** *squares, stripes, horizontal_stripes are mutual exclusive*  
`squares` - squared pattern shirt, boolean `?squares=true`  
`stripes` - vertical stripes pattern shirt, boolean `?stripes=true`  
`horizontal_stripes` - horizontal stripes pattern shirt, boolean `?horizontal_stripes=true`    

`split` - shirt is split into half, boolean `split?=true` **not implemented**
`shirt_type` accept: `short_sleeves`, `long_sleeves`. `shirt_type?=short_sleeves` **not implemented**
`sleeve_detail` **not implemented**
`v_neck` shirt v-neck pattern instead of round one, boolean `v_neck=true`

example request  
http://localhost:9013/svg?sleeve=000000&v_neck=true&squares=true&t=1234&base=ffff00