package main

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestSVGHandler(t *testing.T) {

}

func BenchmarkSVGHandler(b *testing.B) {

	rc := randomSVHHAndlerContext()
	for i := 0; i < b.N; i++ {
		svgHandler(rc)
	}
}

func randomSVHHAndlerContext() *gin.Context {
	gin.SetMode(gin.TestMode)

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request = &http.Request{
		Method: "GET",
		Header: make(http.Header),
		URL:    &url.URL{},
	}
	c.Request.Header.Set("Content-Type", "image/svg+xml")

	// set query params
	u := getRandomQueryParams()

	c.Request.URL.RawQuery = u.Encode()

	return c
}
